CREATE DATABASE IF NOT EXISTS sopr DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
use sopr;

#用户表
CREATE TABLE IF NOT EXISTS sopr_user(
  id INT(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  username VARCHAR(16) UNIQUE NOT NULL,
  password VARCHAR(16) NOT NULL,
  realname VARCHAR(16) NOT NULL,
  is_delete INT(1) DEFAULT 0,
  last_login_time INT(10) DEFAULT NULL,
  create_time INT(10) NOT NULL
);
INSERT INTO sopr_user VALUES (null, 'admin', 'admin', 'yishuangxi', 0, 1451635482, 1451635482);

#角色表
CREATE TABLE IF NOT EXISTS sopr_group(
  id INT(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  gname VARCHAR(16) UNIQUE NOT NULL,
  is_delete INT(1) DEFAULT 0,
  create_time INT(10) NOT NULL
);
INSERT INTO sopr_group VALUES (null, '管理员', 0, 1451635482);
INSERT INTO sopr_group VALUES (null, '普通用户', 0,1451635482);

#功能模块表
CREATE TABLE IF NOT EXISTS sopr_module(
  id INT(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  mkey VARCHAR(16) UNIQUE NOT NULL,
  mname VARCHAR(16) UNIQUE NOT NULL,
  murl VARCHAR(255) UNIQUE NOT NULL,
  is_delete INT(1) DEFAULT 0,
  create_time INT(10) NOT NULL
);

#角色模块关联表
CREATE TABLE IF NOT EXISTS sopr_group_module(
  g_id INT(10) NOT NULL,
  m_id INT(10) NOT NULL,
  PRIMARY KEY (g_id, m_id)
);

#日志表






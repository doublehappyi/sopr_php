<?php

/**
 * Created by PhpStorm.
 * User: yishuangxi
 * Date: 2016/1/1
 * Time: 18:23
 */
class AuthController extends Think\Controller{
    protected function auth(){
        if(!isset($_SESSION['username'])){
            $this->redirect('admin/user/login');
        }
    }
}
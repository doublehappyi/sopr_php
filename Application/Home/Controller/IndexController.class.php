<?php
namespace Home\Controller;

use Think\Controller;

class IndexController extends Controller
{
    public function index()
    {

    }

    public function welcome()
    {
        if (isset($_SESSION['username'])) {
            $this->display();
        } else {
            $this->redirect('admin/user/login');
        }
    }
}
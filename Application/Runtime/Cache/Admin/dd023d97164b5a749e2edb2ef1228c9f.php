<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta name="keywords" content="">
<meta name="description" content="">
<meta http-equiv="Cache-Control" content="no-siteapp" />
    
<!--[if lt IE 9]>
<script type="text/javascript" src="/sopr/Public/lib/html5.js"></script>
<script type="text/javascript" src="/sopr/Public/lib/respond.min.js"></script>
<script type="text/javascript" src="/sopr/Public/lib/PIE_IE678.js"></script>
<![endif]-->
<!--[if IE 6]>
<script type="text/javascript" src="__PULBIC__/lib/DD_belatedPNG_0.0.8a-min.js" ></script>
<script>DD_belatedPNG.fix('*');</script>
<![endif]-->
    <link href="/sopr/Public/lib/hui/css/H-ui.min.css" rel="stylesheet" type="text/css" />
    <link href="/sopr/Public/lib/hui/css/H-ui.login.css" rel="stylesheet" type="text/css" />
    <link href="/sopr/Public/lib/hui/Hui-iconfont/1.0.6/iconfont.css" rel="stylesheet" type="text/css" />
    <title>京东搜索运营平台</title>
</head>
<body>
<div class="header"></div>
<div class="loginWraper">
    <div id="loginform" class="loginBox">
        <form class="form form-horizontal" action="" method="post">
            <div class="row cl">
                <label class="form-label col-3"><i class="Hui-iconfont">&#xe60d;</i></label>
                <div class="formControls col-8">
                    <input id="username" name="username" type="text" placeholder="账户" class="input-text size-L">
                </div>
            </div>
            <div class="row cl">
                <label class="form-label col-3"><i class="Hui-iconfont">&#xe60e;</i></label>
                <div class="formControls col-8">
                    <input id="password" name="password" type="password" placeholder="密码" class="input-text size-L">
                </div>
            </div>
            <!--<div class="row cl">-->
                <!--<div class="formControls col-8 col-offset-3">-->
                    <!--<input class="input-text size-L" type="text" placeholder="验证码" onblur="if(this.value==''){this.value='验证码:'}" onclick="if(this.value=='验证码:'){this.value='';}" value="验证码:" style="width:150px;">-->
                    <!--<img src="images/VerifyCode.aspx.png"> <a id="kanbuq" href="javascript:;">看不清，换一张</a> </div>-->
            <!--</div>-->
            <div class="row">
                <div class="formControls col-8 col-offset-3">
                    <label for="online">
                        <input type="checkbox" name="online" id="online" value="">
                        使我保持登录状态</label>
                </div>
            </div>
            <div class="row">
                <div class="formControls col-8 col-offset-3">
                    <input name="" type="submit" class="btn btn-success radius size-L" value="&nbsp;登&nbsp;&nbsp;&nbsp;&nbsp;录&nbsp;">
                    <input name="" type="reset" class="btn btn-default radius size-L" value="&nbsp;取&nbsp;&nbsp;&nbsp;&nbsp;消&nbsp;">
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript" src="/sopr/Public/lib/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="/sopr/Public/lib/hui/js/H-ui.js"></script>
<script type="text/javascript" src="/sopr/Public/lib/hui/js/H-ui.admin.js"></script>
<script type="text/javascript" src="/sopr/Public/lib/layer/2.1/layer.js"></script>
</body>
</html>